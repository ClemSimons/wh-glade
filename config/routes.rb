Rails.application.routes.draw do

  get "home/index"

  resources :snowboards

  root "home#index"

end
