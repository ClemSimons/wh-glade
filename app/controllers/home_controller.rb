class HomeController < ApplicationController

  def index
    @home_url = request.base_url
  end

end
