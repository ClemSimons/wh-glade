
class Carousel {

  constructor (element, options = {}) {
    // Carousel constructor
    this.element = element
    this.options = Object.assign({}, {
      slidesToScroll: 1,
      slidesVisible: 1,
    }, options)
    let children = [].slice.call(element.children)
    this.isMobile = false
    this.currentItem = 0
    this.root = this.createDivWithClass("carousel")
    this.container = this.createDivWithClass("carousel_item_container")
    this.root.appendChild(this.container)
    this.element.appendChild(this.root)
    this.items = children.map((child) => {
      let item = this.createDivWithClass("carousel_item")
      item.appendChild(child)
      this.container.appendChild(item)
      return item
    })
    this.setStyle()
    this.createNavigation()
    this.onWindowResize()
    window.addEventListener('resize', this.onWindowResize.bind(this))
  }

  createDivWithClass (className) {
    // create a div with the given className
    let div = document.createElement("div")
    div.setAttribute("class", className)
    return div
  }

  setStyle () {
    // set style for each carousel items
    let ratio = this.items.length / this.slidesVisible
    this.container.style.width = ((ratio) * 100) + "%"
    this.items.forEach(item => item.style.width = ((100 / this.slidesVisible) / ratio) + "%")
  }

  createNavigation () {
    // create prev and next buttons and call functions 'prev' or 'next' on click
    let nextButton = this.createDivWithClass("carousel_next")
    let prevButton = this.createDivWithClass("carousel_prev")
    document.querySelector("#carousel_right_arrow").appendChild(nextButton)
    document.querySelector("#carousel_left_arrow").appendChild(prevButton)
    nextButton.addEventListener("click", this.next.bind(this))
    prevButton.addEventListener("click", this.prev.bind(this))
  }

  next () {
    // go to next item
    this.goToItem(this.currentItem + this.slidesToScroll)
  }

  prev () {
    // go to prev item
    this.goToItem(this.currentItem - this.slidesToScroll)
  }

  goToItem (index) {
    // go to an item
    if (index < 0) {
      index = this.items.length - this.options.slidesVisible
    } else if (index > this.items.length || (this.items[this.currentItem + this.options.slidesVisible] === undefined && index > this.currentItem)) {
      index = 0
    }
    let translateX = index * -100 / this.items.length
    this.container.style.transform = 'translate3d(' + translateX + '%, 0, 0)'
    this.currentItem = index
  }

  onWindowResize () {
    // check window width on resize and change mobile variable and style accordingly
    let mobile = window.outerWidth < 1050
    if (mobile != this.isMobile) {
      this.isMobile = mobile
      this.setStyle()
    }
  }

  get slidesToScroll () {
    // getter in order to change slidesToScroll value if on mobile
    return this.isMobile ? 1 : this.options.slidesToScroll
  }

  get slidesVisible () {
    // getter in order to change slidesVisible value if on mobile
    return this.isMobile ? 1 : this.options.slidesVisible
  }

}

document.addEventListener("DOMContentLoaded", function () {
  // once DOM content is loaded, create Carousel
  new Carousel(document.querySelector("#carousel_container"), {
    slidesToScroll: 1,
    slidesVisible: 5
  })

})
